import math


def m2(n, e, n0, e0):
    cn = n - n0
    ce = e - e0
    dst = math.hypot(cn, ce)
    if dst == 0:
        azi = 0
    else:
        if ce < 0:
           azi = 360 - math.degrees(math.acos(cn/dst))
        else:
            azi = math.degrees(math.acos(cn / dst))
    return azi, dst


def measure(n, e, r, n0, e0):
    az, dst = m2(n, e, n0, e0)
    azi = az - r
    if azi < 0:
        azi = azi+360
    return azi, dst

# test


#Center of target
e0 = 5000
n0 = 5000

#North direction of target 45
ea = 2813.55
na = 7428.30

#M1 88
e1 = 7580.93
n1 = 2593.24


#M2 143.7359
e2 = 1611.77
n2 = 2054.66

#M3 247.4065
e3 = 2813.55
n3 = 7428.30

#M4 345.5404
e4 = 5000
n4 = 7000

#North direction of target 45
e5 = 7453.65
n5 = 7453.65


r, d = m2(na, ea, n0, e0)

d1, a1 = measure(n1, e1, r, n0, e0)
d2, a2 = measure(n2, e2, r, n0, e0)
d3, a3 = measure(n3, e3, r, n0, e0)
d4, a4 = measure(n4, e4, r, n0, e0)
d5, a5 = measure(n5, e5, r, n0, e0)

print(" Rotation: ", round(r,3))
print("Azi1: ", round(d1,3), " dist1: ", round((a1),3))
print("Azi2: ", round(d2,3), " dist2: ", round((a2),3))
print("Azi3: ", round(d3,3), " dist3: ", round((a3),3))
print("Azi4: ", round(d4,3), " dist4: ", round((a4),3))
print("Azi5: ", round(d5,3), " dist5: ", round((a5),3))
