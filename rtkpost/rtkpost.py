################################################################################
# RTK PostProcessor
#
# Converting raw to rinex for marker_id = 192168000001
#
# Input parameters:
# - marker ID
# - Base station Lat, Lon, Hgt
#
# Input files:
# - Raw data from base base_[marker_id].ubx
# - Raw data from marker marker_[marker_id].ubx
#
# Output file:
# - marker_[marker_id].pos solution file with ENU formatting
#
# python3 rtkpost.py  -log_base base_192168000001 -log_marker marker_192168000001
#       -out OUT_CP -marker_id 192168000001 -base_lat 48.434804545 -base_lon 35.039088835
#       -base_hgt 165.99
################################################################################

from subprocess import Popen
import sys
import os.path
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-log_base", dest="log_base", type=str, help="log base file path")
parser.add_argument("-log_marker", dest="log_marker", type=str, help="log marker file path")
parser.add_argument("-out", dest="out", type=str, help="out file path")
parser.add_argument("-cache", dest="cache", type=str, help="cache")
parser.add_argument("-marker_id", dest="marker_id", type=str, help="marker_id")
parser.add_argument("-base_lat", dest="base_lat", type=str, help="base_lat")
parser.add_argument("-base_lon", dest="base_lon", type=str, help="base_lon")
parser.add_argument("-base_hgt", dest="base_hgt", type=str, help="base_hgt")
args = parser.parse_args()

marker_id = args.marker_id#'192168000001'
base_lat = args.base_lat#'48.434804545'
base_lon = args.base_lon#'35.039088835'
base_hgt = args.base_hgt#'165.99'

ubxraw = args.log_base + '.ubx'
if (os.path.exists(ubxraw)):
    cmd = 'convbin ' + ubxraw + ' -r ubx -v 3.03 -od -os -hm BASE'
    print(cmd)
    p = Popen(cmd, shell=True)
    try:
        p.wait(timeout=120)
    except TimeoutExpired:
        p.kill()
        print('Base Raw2Rinex conversion timeout')


ubxraw = args.log_marker + '.ubx'
if (os.path.exists(ubxraw)):
    cmd = 'convbin ' + ubxraw + ' -r ubx -v 3.03 -od -os -hm '+marker_id
    print(cmd)
    p = Popen(cmd, shell=True)
    try:
        p.wait(timeout=120)
    except TimeoutExpired:
        p.kill()
        print('marker Raw2Rinex conversion timeout')

cmd = 'rnx2rtkp -k ../rtkpost/rtkpost.conf -navc {CACHE} -l {lat} {lon} {hgt} -o {OUT}.pos {LOG_MARKER}.obs {LOG_BASE}.obs {LOG_BASE}.nav {LOG_MARKER}.nav'
cmd = cmd.format(OUT=args.out, LOG_BASE=args.log_base, LOG_MARKER=args.log_marker, CACHE=args.cache, lat=base_lat, lon=base_lon, hgt=base_hgt)
print(cmd)
p = Popen(cmd, shell=True)
try:
    p.wait(timeout=120)
except TimeoutExpired:
    p.kill()
    print('RTK postprocessing timeout')
