import re
from sys import *
path.append("../tcp")
from tcp_server import *
from tcp_client import *
from subprocess import *
from rtkrcv_out_reader import *
from time import *
################################################################################
ar_opt = None
ar_in1 = None
ar_in2 = None
ar_out1 = None
ar_logpath1 = None#marker
ar_logpath2 = None#base
ar_log_out = None#out1

ar_gout1 = None
ar_ip = None
ar_ratio = None
ar_Q = None
ar_mid = None
is_console_start = 0
################################################################################
def init():
    global ar_mode
    global is_console_start
    global ar_opt
    global ar_in1
    global ar_in2
    global ar_out1
    global ar_logpath1#marker
    global ar_logpath2#base
    global ar_log_out#out1
    global ar_gout1
    global is_console_start
    global ar_ip
    global ar_ratio
    global ar_Q
    global ar_mid

    if '-mode' in argv:
        print (argv[(argv.index('-mode')+1)])
        ar_mode = str(argv[(argv.index('-mode')+1)])
        is_console_start = is_console_start + 1

    if '-o' in argv:
        print (argv[(argv.index('-o')+1)])
        ar_opt = str(argv[(argv.index('-o')+1)])
        is_console_start = is_console_start + 1

    if '-in1' in argv:
        print (argv[(argv.index('-in1')+1)])
        ar_in1 = str(argv[(argv.index('-in1')+1)])
        is_console_start = is_console_start + 1

    if '-in2' in argv:
        print (argv[(argv.index('-in2')+1)])
        ar_in2 = str(argv[(argv.index('-in2')+1)])
        is_console_start = is_console_start + 1

    if '-out1' in argv:
        print (argv[(argv.index('-out1')+1)])
        ar_out1 = str(argv[(argv.index('-out1')+1)])
        is_console_start = is_console_start + 1

    if '-log1' in argv:
        print (argv[(argv.index('-log1')+1)])
        ar_logpath1 = str(argv[(argv.index('-log1')+1)])
        is_console_start = is_console_start + 1

    if '-log2' in argv:
        print (argv[(argv.index('-log2')+1)])
        ar_logpath2 = str(argv[(argv.index('-log2')+1)])
        is_console_start = is_console_start + 1

    if '-log_out' in argv:
        print (argv[(argv.index('-log_out')+1)])
        ar_log_out = str(argv[(argv.index('-log_out')+1)])
        is_console_start = is_console_start + 1

    if '-g' in argv:
        print (argv[(argv.index('-g')+1)])
        ar_gout1 = str(argv[(argv.index('-g')+1)])
        is_console_start = is_console_start + 1

    if '-ip' in argv:
        print (argv[(argv.index('-ip')+1)])
        ar_ip = str(argv[(argv.index('-ip')+1)])
        is_console_start = is_console_start + 1

    if '-r' in argv:
        print (argv[(argv.index('-r')+1)])
        ar_ratio = str(argv[(argv.index('-r')+1)])
        is_console_start = is_console_start + 1

    if '-q' in argv:
        print (argv[(argv.index('-q')+1)])
        ar_Q = str(argv[(argv.index('-q')+1)])
        is_console_start = is_console_start + 1

    if '-m' in argv:
        print (argv[(argv.index('-m')+1)])
        ar_mid = str(argv[(argv.index('-m')+1)])
        is_console_start = is_console_start + 1

    if '-h' in argv:
        print ('test_tag.py [-sp [serial port address][-b [baudrate]]] [-c [count of tags to create, 1 default]] [-hp [host:port]]')
################################################################################
init()
################################################################################
class Rtkrcv_start(object):
    def set_option(self, opt_file, opt_line, opt_name, value):
        if (value == None): return
        line0 = opt_name + "       =" + value + "\n"
        data = open(opt_file).read()
        o = open(opt_file,'w')
        o.write( re.sub(opt_line,line0,data)  )
        o.close()
    def __init__(self, _opt_file, _in1, _in2, _out1, _log1, _log2):
        in1  = "inpstr2-path"
        in2  = "inpstr1-path"
        out1 = "outstr1-path"
        log1 = "logstr1-path"
        log2 = "logstr2-path"
        self.tport = str(_in2).split(":")
        self.tport = int(self.tport[1]) - 1000
        self.tport = str(self.tport)
        rtkrcv = None
        with open(_opt_file, "r+") as file:
            lines = file.readlines()
            for i in range(0, len(lines)):
                line = str(lines[i])
                if in1 in line:
                    self.set_option(_opt_file,line,in1,_in1)
                if in2 in line:
                    self.set_option(_opt_file,line,in2,_in2)
                if out1 in line:
                    self.set_option(_opt_file,line,out1,_out1)
                if log1 in line:
                    self.set_option(_opt_file,line,log1,_log1)
                if log2 in line:
                    self.set_option(_opt_file,line,log2,_log2)

        command = "rtkrcv -s "+ "-p " +str(self.tport) +" -o "+_opt_file + " -w \"\""
        self.rtkrcv = Popen(command,shell=True)

    def rtkrcv_close(self):
        cli = Client_tcp("killer", "localhost", int(self.tport))
        cli.client_write("shutdown\n\r") 
        #self.rtkrcv.kill()
        #self.rtkrcv.wait()

    def get_telnet(self):
        return self.tport
################################################################################
def kill_port(port):
    port_killer_cmd = "fuser -k -n tcp %s" % str(port)
    kill = Popen(port_killer_cmd, shell=True, stdin=PIPE, stdout=PIPE)
    kill.wait()
################################################################################
def log_post(self, msg):
    data = {
      "flight_id": "0000",
      "message": msg,
      "sender": "rtkrcv out reader"
    }
    resp = requests.post('http://localhost:5000/api/log', json.dumps(data), headers={"Content-Type": "application/json"})

def console_starter():
    global ar_mode
    global ar_opt
    global ar_in1
    global ar_in2
    global ar_out1
    global ar_logpath1#marker
    global ar_logpath2#base
    global ar_log_out#out1
    global ar_gout1
    global ar_ip
    global ar_ratio
    global ar_Q
    global ar_mid

    if(ar_logpath1!=None)and(ar_logpath2!=None):
        rtkrcv = Rtkrcv_start(ar_opt, ar_in1, ar_in2, ar_out1, (str(ar_logpath2)+".ubx"), (str(ar_logpath1+".ubx")))
    else:
        rtkrcv = Rtkrcv_start(ar_opt, ar_in1, ar_in2, ar_out1, ar_logpath1, ar_logpath2)

    sleep(5)
    log_post(rtkrcv.get_telnet())
    reader = Rtkrcv_out_reader(ar_ip, ar_out1, ar_gout1, ar_ratio, ar_Q, ar_mid, ar_logpath2, ar_logpath1, ar_log_out)
    if ar_mode == "single":
        reader.get_fix_single()
        rtkrcv.rtkrcv_close()
    else:
        reader.get_fix()
        rtkrcv.rtkrcv_close()
        host_port = ar_in2.split(":")
        while True:
            try:
                sendman_srv = Server_tcp(str(host_port[0]), int(host_port[1]))#start sendman server
                #sendman_srv = Server_tcp("192.168.0.22", 7000)#start sendman server
                break
            except Exception:
                kill_port(int(host_port[1]))
        sendman_srv.server_listen(1)
        sendman_srv.server_accept()
        while True:
            sendman_srv.server_send("POWER_OFF")#send SLEEP$ to tag
            if sendman_srv.server_rcv() == "Device is off":
                break
            else:
                sendman_srv.server_accept()

        sendman_srv.server_close()#kill sendman server"""

if is_console_start != 0:
    console_starter()
