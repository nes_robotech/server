from datetime import datetime
from math import *
from subprocess import *
import time
import requests
import json
################################################################################
#rtkrcv_out_reader class
# _ip               - tag ip
# _out_file         - patn to rtkrcv out file
# _global_file      - path to GLOBAL output file
# _exp_ratio        - expected ratio in rtkrcv out file
# _exp_Q            - expected Q in rtkrcv out file
# _exp_mid          - number of semples
################################################################################
class Rtkrcv_out_reader(object):
    log_out = None
    log_base = None
    log_marker = None
    exp_mid = None
    exp_Q = None
    exp_ratio = None
    global_file = None
    out_file = None
    out_f = None
    out_f_post = None
    lines = []
    lines_post = []
    line_num = 2.0
    line_num_post = 0.0
    E = 0.0
    N = 0.0
    U = 0.0
    Q = 0
    ratio = 0.0
    mid = 0.0

    E_post = 0.0
    N_post = 0.0
    U_post = 0.0
    Q_post = 0
    ratio_post = 0.0
    mid_post = 0.0

    l_2d = None
    l_3d = None
    ip = None
    def __init__(self, _ip, _out_file, _global_file, _exp_ratio, _exp_Q, _exp_mid, _log_marker, _log_base, _log_out):
        self.log_out = _log_out
        self.log_base = _log_base
        self.log_marker = _log_marker
        self.ip = _ip
        self.exp_ratio = _exp_ratio
        self.exp_Q = _exp_Q
        self.exp_mid = _exp_mid
        self.line_num = 2.0
        self.global_file = _global_file
        self.out_file = _out_file
        try:
            self.out_f = open(str(self.out_file),'r+')
            self.lines = self.out_f.readlines()
        except Exception:
            pass

    def get_timestamp(self):
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    def log_post(self, msg):
        data = {
          "flight_id": "0000",
          "message": msg,
          "sender": "rtkrcv out reader"
        }
        resp = requests.post('http://localhost:5000/api/log', json.dumps(data), headers={"Content-Type": "application/json"})
        assert resp.json() == {'status': 'Ok'}, 'AP status update error'


    def remove_blank(self,string):
        arr = string.split(' ')
        n = arr.count('')
        if n == 0:
            return None
        while True:
            if n == 0:
                break
            arr.remove('')
            n = n-1
        return arr

    def generate_rtkpost_file(self):
        base_llh_file = "../rtk/BASE/BASE.txt"
        cache = "../rtk/rtkrcv.nav"
        single_rs_f = open(base_llh_file,"r")
        single_rs_f_data = str(single_rs_f.read())
        single_rs_f_data_list = single_rs_f_data.split(':')
        rtkpost_cmd = "python3 ../rtkpost/rtkpost.py -cache {CACHE}  -log_base {LOG_BASE} -log_marker {LOG_MARKER} -out {OUT} -marker_id {ID} -base_lat {LAT} -base_lon {LON} -base_hgt {HGT}".format(
        CACHE="../api/rtkrcv.nav", LOG_BASE=self.log_base, LOG_MARKER=self.log_marker, OUT=self.log_out, ID=self.ip, LAT=str(single_rs_f_data_list[1]), LON=str(single_rs_f_data_list[2]), HGT=str(single_rs_f_data_list[3]))
        rtkpost = Popen(rtkpost_cmd,bufsize=0,shell=True,stdout=PIPE)
        rtkpost.wait()
        self.line_num_post = 0.0
        self.out_f_post = open((str(self.log_out)+".pos"),'r+')
        self.lines_post = self.out_f_post.readlines()

    def get_line(self):
        while True:
            glo_f = open((str(self.global_file)+"_debug.txt"), "a")
            glo_f.write("line_Q:" + str(self.Q) + " ratio:" + str(self.ratio) + " mid:" + str(self.mid) + "\r\n")
            glo_f.write("Qexp:" + str(self.exp_Q) + " ratioexp:" + str(self.exp_ratio) + " midexp:" + str(self.exp_mid) + "\r\n")
            glo_f.write(str(self.out_file))
            try:
                line = self.lines[int(self.line_num)]
                self.line_num = self.line_num + 1.0
                return line
            except Exception:
                self.out_f = open(str(self.out_file),'r+')
                self.lines = self.out_f.readlines()
                glo_f.write("EOF\r\n")
                return "EOF"
            glo_f.close()

    def get_line_post(self):
        while True:
            try:
                line = self.lines_post[int(self.line_num_post)]
                self.line_num_post = self.line_num_post + 1.0
                return line
            except Exception:
                try:
                    self.line_num_post = 0.0
                    self.out_f_post = open((str(self.log_out)+".pos"),'r+')
                    self.lines_post = self.out_f_post.readlines()
                    return "EOF"
                except Exception:
                    return "EOF"

    def proc_line(self, _line):
        try:
            self.ratio = float(_line[14])
            self.Q = int(_line[5])
            #self.log_post("line_num: "+str(self.line_num_post)+str(_line))
            #self.log_post ("line_num: "+str(self.line_num_post)+"ratio: "+str(self.ratio)+" exp_ratio: "+str(self.exp_ratio)+" Q: "+str(self.Q)+" exp_Q: "+str(self.exp_Q)+" mid: "+str(self.mid)+" exp_mid: "+str(float(self.exp_mid))+"\r\n")
            glo_f = open((str(self.global_file)+"_debug.txt"), "a")
            glo_f.write("Q:" + str(self.Q) + " ratio:" + str(self.ratio) + " mid:" + str(self.mid) + "\r\n")
            glo_f.write("line_num: "+str(self.line_num)+" "+str(_line) + "\r\n")
            glo_f.close()
            if (float(self.ratio) >= float(self.exp_ratio)):
                if (int(self.Q) <= int(self.exp_Q)):
                    self.mid = float(self.mid) + 1.0
                    self.E = float(self.E) + abs(float(_line[2]))
                    self.N = float(self.N) + abs(float(_line[3]))
                    self.U = float(self.U) + abs(float(_line[4]))
                    if (float(self.mid) == float(self.exp_mid)):
                        self.E = self.E / self.mid
                        self.N = self.N / self.mid
                        self.U = self.U / self.mid
                        #self.l_2d = sqrt(( (self.E*self.E)+(self.N*self.N) ))#2D
                        #self.l_3d = sqrt(( (self.E*self.E)+(self.N*self.N)+(self.U*self.U) ))#3D
                        glo_str = str(self.ip) + ":" + str(self.E) + ":" + str(self.N) + ":" + str(self.U) + "\n"# + ":" + str(self.l_2d) + ":" + str(self.l_3d) + "\n"
                        glo_f = open(str(self.global_file), "a")
                        glo_f.write(glo_str)
                        glo_f.close()
                        return "fix"
                else:
                    self.mid = 0
                    self.E = 0
                    self.N = 0
                    self.U = 0
            else:
                self.mid = 0
                self.E = 0
                self.N = 0
                self.U = 0
        except Exception:
            pass

    def proc_line_rtkpost(self, _line):
        try:
            self.ratio_post = float(_line[14])
            self.Q_post = int(_line[5])
            #self.log_post("line_num: "+str(self.line_num_post)+str(_line))
            #self.log_post ("line_num: "+str(self.line_num_post)+"ratio_post: "+str(self.ratio_post)+" exp_ratio: "+str(self.exp_ratio)+" Q_post: "+str(self.Q_post)+" exp_Q: "+str(self.exp_Q)+" mid_post: "+str(self.mid_post)+" exp_mid: "+str(float(self.exp_mid))+"\r\n")
            #print("Q:" + str(self.Q_post) + " ratio:" + str(self.ratio_post) + " mid:" + str(self.mid_post))
            glo_f = open((str(self.global_file)+"_debug.txt"), "a")
            glo_f.write("Q:" + str(self.Q_post) + " ratio:" + str(self.ratio_post) + " mid:" + str(self.mid_post) + "\r\n")
            glo_f.write("line_num: "+str(self.line_num_post)+" "+str(_line) + "\r\n")
            glo_f.close()
            if (float(self.ratio_post) >= float(self.exp_ratio)):
                if (int(self.Q_post) <= int(self.exp_Q)):
                    self.mid_post = float(self.mid_post) + 1.0
                    self.E_post = float(self.E_post) + abs(float(_line[2]))
                    self.N_post = float(self.N_post) + abs(float(_line[3]))
                    self.U_post = float(self.U_post) + abs(float(_line[4]))
                    if (float(self.mid_post) == float(self.exp_mid)):
                        self.E_post = self.E_post / self.mid_post
                        self.N_post = self.N_post / self.mid_post
                        self.U_post = self.U_post / self.mid_post
                        #self.l_2d = sqrt(( (self.E*self.E)+(self.N*self.N) ))#2D
                        #self.l_3d = sqrt(( (self.E*self.E)+(self.N*self.N)+(self.U*self.U) ))#3D
                        glo_str = str(self.ip) + ":" + str(self.E_post) + ":" + str(self.N_post) + ":" + str(self.U_post) + "\n"# + ":" + str(self.l_2d) + ":" + str(self.l_3d) + "\n"
                        glo_f = open(str(self.global_file), "w")
                        glo_f.write(glo_str)
                        glo_f.close()
                        return "fix"
                else:
                    self.mid_post = 0.0
                    self.E_post = 0.0
                    self.N_post = 0.0
                    self.U_post = 0.0
            else:
                self.mid_post = 0.0
                self.E_post = 0.0
                self.N_post = 0.0
                self.U_post = 0.0
        except Exception:
            self.log_post("error")

    def get_fix_post(self):
        self.generate_rtkpost_file()
        while True:
            line = self.get_line_post()
            if (line == "EOF"):
                return "EOF"
            arr = self.remove_blank(line)
            if(arr != None) and (arr[0] != "%"):
                if(self.proc_line_rtkpost(arr)=="fix"):
                    return "fix"

    def get_fix_realtime(self):
        while True:
            line = self.get_line()
            if (line == "EOF"):
                return "EOF"
            arr = self.remove_blank(line)
            if(arr != None) and (arr[0] != "%"):
                if(self.proc_line(arr)=="fix"):
                    return "fix"

    def get_fix(self):
        time.sleep(15)
        while True:
            time.sleep(5)
            if(self.get_fix_post()=="fix"):
                break

    def get_fix_single(self):
        while True:
            time.sleep(5)
            if(self.get_fix_realtime()=="fix"):
                break


################################################################################

"""
reader = Rtkrcv_out_reader("000B", "CP/CP_OUT_LOG.pos", "CP/GLOBAL.txt", 3.0, 1, 10, "CP/CP_BASE_LOG", "CP/CP_MARKER_LOG", "CP/CP_OUT_LOG")

reader.line_num_post = 0
reader.out_f_post = open((str(reader.log_out)+".pos"),'r+')
reader.lines_post = reader.out_f_post.readlines()

reader.get_fix()


while True:
    line = reader.get_line_post()
    if (line == "EOF"):
        print("EOF")
        break

    arr = reader.remove_blank(line)
    if(arr != None) and (arr[0] != "%"):
        answer = reader.proc_line_rtkpost(arr)
        if(answer=="fix"):
            print( "fix")
"""
