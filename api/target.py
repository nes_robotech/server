from datetime import datetime
from subprocess import Popen

from flask import abort
#ToDo Replace all abort call by return
#ToDo Replace HTTP Status Codes acconding to the https://opensource.zalando.com/restful-api-guidelines/#150

TARGET = {}


def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def create(flight_id, target_id):

    target_found = False
    if flight_id not in TARGET:
        TARGET[flight_id]={'target':[]}
    for i in range(0, len(TARGET[flight_id]['target'])):
        if TARGET[flight_id]['target'][i]['target_id'] == target_id:
            target_found = True
            break
    if not target_found:
        TARGET[flight_id]['target'].append({
            'target_id': target_id,
            "rs_state": "n/a",
            "rs_llh": {
                "lat": 0,
                "lon": 0,
                "hgt": 0
            },
            "cp_state": "n/a",
            "cp_llh": {
                "lat": 0,
                "lon": 0,
                "hgt": 0
            },
            "ap_state": "n/a",
            "ap_llh": {
                "lat": 0,
                "lon": 0,
                "hgt": 0
            },
            'timestamp': get_timestamp()
        })
        return {"status":"Ok"}, 200

    else:
        abort(406, {"status": "Error", "message": "Target {target_id} already exists".format(
            target_id=target_id)})


def ap_init(flight_id, target_id, marker_id):
    """target_ap_init

    Initialization of target azimuth point for specified &#x60;target_id&#x60;

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param marker_id: Marker identifier
    :type marker_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0, len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:

                    #TODO Wait for marker registration
                    #TODO Start rtknavi to collect ubx data
                    #TODO Start PostProc to define ap_llh
                    #TODO Start cp_state updater
                    TARGET[flight_id]['target'][i]['ap_marker_id'] = marker_id
                    TARGET[flight_id]['target'][i]['ap_state'] = 'init'

                    #Test loop
                    cmd = "python3 ap_init.py -flight_id {flight_id} -target_id {target_id} -marker_id {marker_id}".format(flight_id=flight_id, target_id=target_id, marker_id=marker_id)
                    try:
                        p = Popen(cmd, shell=True)
                    except TimeoutExpired:
                        print('AP init test fault!')

                    #End of test loop

                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok"}, 200

    else:
        abort(401, {"status":"Error", "message": "Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def ap_state(flight_id, target_id):
    """target_ap_state

    Read the state of target azimuth point for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0, len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:
                    state = TARGET[flight_id]['target'][i]['ap_state']
                    pos = TARGET[flight_id]['target'][i]['ap_llh']
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok",
                "state": state,
                "llh": pos}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def cp_init(flight_id, target_id, marker_id):
    """target_cp_init

    Initialization of target central point for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param marker_id: Marker identifier
    :type marker_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0, len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:
                    #TODO Wait for marker registration
                    #TODO Start rtknavi to collect ubx data
                    #TODO Start PostProc to define cp_llh
                    #TODO Start cp_state updater
                    TARGET[flight_id]['target'][i]['cp_marker_id'] = marker_id
                    TARGET[flight_id]['target'][i]['cp_state'] = 'init'

                    #Test loop
                    cmd = "python3 cp_init.py -flight_id {flight_id} -target_id {target_id} -marker_id {marker_id}".format(flight_id=flight_id, target_id=target_id, marker_id=marker_id)
                    try:
                        p = Popen(cmd, shell=True)
                    except Exception:
                        print('CP init test fault!')

                    #End of test loop

                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok"}, 200

    else:
        abort(401, {"status":"Error", "message": "Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def cp_state(flight_id, target_id):
    """target_cp_state

    Read the state of target center point for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0,len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:
                    state = TARGET[flight_id]['target'][i]['cp_state']
                    pos = TARGET[flight_id]['target'][i]['cp_llh']
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok",
                "state": state,
                "llh": pos}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def readlist(flight_id):
    """target_readlist

    Read the list of target for specified &#x60;flight_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    """

    if flight_id in TARGET and flight_id is not None:
        targetlist=TARGET.get(flight_id)
        return {"status": "Ok",
                "target": targetlist.get('target')}, 200

    else:
        abort(406, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def readstatus(flight_id, target_id):
    """target_readstatus

    Read the status of target for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0,len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:
                    target_state = TARGET[flight_id]['target'][i]
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok",
                "target": target_state}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def rs_run(flight_id, target_id, rs_id):
    """target_rs_run

    Run the reference station for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param rs_id: RS identifier
    :type rs_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0, len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:
                    #TODO Start str2str to receive RS stream
                    #TODO Start rtknavi in single mode to define rs_llh
                    #TODO Start rs_state updater
                    TARGET[flight_id]['target'][i]['rs_id'] = rs_id
                    TARGET[flight_id]['target'][i]['rs_state'] = 'init'

                    #Test loop
                    cmd = "python3 rs_init.py -flight_id {flight_id} -target_id {target_id} -rs_id {rs_id}".format(
                        flight_id=flight_id, target_id=target_id, rs_id=rs_id)
                    try:
                        p = Popen(cmd, shell=True)
                    except Exception:
                        print('RS init test fault!')

                    #End of test loop

                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok"}, 200

    else:
        abort(401, {"status":"Error", "message": "Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def rs_state(flight_id, target_id):
    """target_rs_state

    Read the state of reference station for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0,len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                if True:
                    state = TARGET[flight_id]['target'][i]['rs_state']
                    pos = TARGET[flight_id]['target'][i]['rs_llh']
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok",
                "state": state,
                "llh": pos}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def rs_update(flight_id, target_id, state):
    """target_rs_update

    Update reference station data &amp; state for specified &#x60;target_id&#x60; # noqa: E501

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param state: RS state and position
    :type state: dict | bytes
    """
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0,len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                #ToDo validate RS state schema
                if True:
                    new_state = state.get('state', None)
                    new_pos = state.get('llh', None)
                    if new_state is not None:
                        TARGET[flight_id]['target'][i]['rs_state'] = new_state
                    if new_pos is not None:
                        TARGET[flight_id]['target'][i]['rs_llh'] = new_pos
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok"}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def ap_update(flight_id, target_id, state):
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0,len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                #ToDo validate AP state
                if True:
                    new_state = state.get('state', None)
                    new_pos = state.get('llh', None)
                    if new_state is not None:
                        TARGET[flight_id]['target'][i]['ap_state'] = new_state
                    if new_pos is not None:
                        TARGET[flight_id]['target'][i]['ap_llh'] = new_pos
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok"}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})


def cp_update(flight_id, target_id, state):
    if flight_id in TARGET and flight_id is not None:
        target_found = False
        for i in range(0,len(TARGET[flight_id]['target'])):
            if TARGET[flight_id]['target'][i]['target_id'] == target_id:
                target_found = True
                #ToDo validate CP state
                if True:
                    new_state = state.get('state', None)
                    new_pos = state.get('llh', None)
                    if new_state is not None:
                        TARGET[flight_id]['target'][i]['cp_state'] = new_state
                    if new_pos is not None:
                        TARGET[flight_id]['target'][i]['cp_llh'] = new_pos
                break
        if not target_found:
            abort(402, {"status": "Error", "message": "Target {target_id} not exists".format(
                target_id=target_id)})
        return {"status": "Ok"}, 200

    else:
        abort(401, {"status":"Error", "message":"Flight {flight_id} not exists".format(
            flight_id=flight_id)})
