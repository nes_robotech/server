import unittest
import requests


class TestTargetCreate(unittest.TestCase):
    def test_target_create(self):
        resp = requests.post('http://localhost:5000/api/target?flight_id=0000&target_id=1234')
        self.assertEqual(resp.json(), {'status': 'Ok'})

    def test_rs_run(self):
        resp = requests.post('http://localhost:5000/api/target/1234/rs?flight_id=0000&target_id=1234')
        print(resp)
        print(resp.json())
        self.assertEqual(resp.json(), {'status': 'Ok'})



if __name__ == "__main__":
    unittest.main()