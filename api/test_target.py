import requests
import pprint
import json
import time

def test_create():
    resp = requests.post('http://localhost:5000/api/target?flight_id=0000&target_id=1234')
    print(resp)
    print(resp.json())
    assert resp.json() == {'status': 'Ok'}, 'Target creation error'

def test_rs_run():
    resp = requests.post('http://localhost:5000/api/target/1234/rs?flight_id=0000&rs_id=0001')
    print(resp)
    print(resp.json())
    assert resp.json() == {'status': 'Ok'}, 'Reference station run error'

def test_ap_run():
    resp = requests.post('http://localhost:5000/api/target/1234/ap?flight_id=0000&marker_id=0002')
    print(resp)
    print(resp.json())
    assert resp.json() == {'status': 'Ok'}, 'Azimuth point init error'

def test_cp_run():
    resp = requests.post('http://localhost:5000/api/target/1234/cp?flight_id=0000&marker_id=0002')
    print(resp)
    print(resp.json())
    assert resp.json() == {'status': 'Ok'}, 'Central point init error'

def test_readlist():
    resp = requests.get('http://localhost:5000/api/target?flight_id=0000')
    print(resp)
    pprint.pprint(resp.json())
    assert 'Ok' in resp.json()['status'], 'Read operation error'

def test_rs_update():
    data = {
        "llh": {
            "hgt": 119.0,
            "lat": 40.1,
            "lon": 30.5
        },
        "state": "test"
    }

    resp = requests.put('http://localhost:5000/api/target/1234/rs?flight_id=0000', json.dumps(data),
                        headers={"Content-Type": "application/json"})
    print(resp)
    print(resp.json())
    assert resp.json() == {'status': 'Ok'}, 'Reference station status update error'

def test_ap_update():
    data = {
        "llh": {
            "hgt": 119.0,
            "lat": 40.1,
            "lon": 30.5
        },
        "state": "test"
    }

    resp = requests.put('http://localhost:5000/api/target/1234/ap?flight_id=0000', json.dumps(data),
                        headers={"Content-Type": "application/json"})
    print(resp)
    print(resp.json())
    assert resp.json() == {'status': 'Ok'}, 'Azimuth point status update error'

def test_rs_state():
    resp = requests.get('http://localhost:5000/api/target/1234/rs?flight_id=0000&target_id=1234')
    print(resp)
    print(resp.json())
    assert 'Ok' in resp.json()['status'], 'Reference station read status error'

def test_target_state():
    resp = requests.get('http://localhost:5000/api/target/1234?flight_id=0000&target_id=1234')
    print(resp)
    print(resp.json())
    assert 'Ok' in resp.json()['status'], 'Target status read error'

test_create()
test_rs_run()
test_ap_run()
test_cp_run()
time.sleep(2)
test_readlist()
time.sleep(5)
test_readlist()
time.sleep(10)
test_readlist()
