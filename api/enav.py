import requests


#GET https://<event>.enavigator.org/api/v1/event/
#GET https://<event>.enavigator.org/api/v1/list/?parent_id= qwerty55&expand=0
#GET https://<event>.enavigator.org/api/v1/participants/?parent_id=sghjqdw86
#GET https://<event>.enavigator.org/api/v1/flight/?flight_id=sghjqdw86
#GET https://<event>.enavigator.org/api/v1/flight/
 
#POST https://<event>.enavigator.org/api/v1/flight/
#POST https://<event>.enavigator.org/api/v1/track/
#POST https://<event>.enavigator.org/api/v1/track/
#POST https://<event>.enavigator.org/api/v1/track/

class Enav(object):
    ###################################################################################################################
    headers_get = {
        'Version': 'v1',
        'Authorization': 'Bearer es_test'
    }
    headers_post = {
        'Content-Type':'application/json',
        'Version': 'v1',
        'Authorization': 'Bearer es_test'
    }
    ###################################################################################################################
    flight_id_struct = {
        "flight_id": None
    }
    lap_struct = {
        "type": None,
        "flight_id": None,
        "transponder_id": None,
        "data":
            {
                "lap": None,
                "time": None
            }
    }
    waypoint_struct = {
        "type": None,
        "flight_id": None,
        "transponder_id": None,
        "data":
            {
                "gate_id": None,
                "time": None
            }
    }
    accuracy_struct = {
        "type": None,
        "flight_id": None,
        "transponder_id": None,
        "data":
            {
                "gate_id": None,
                "distance": None,
                "time": None,
                "azimuth": None
            }
    }
    ###################################################################################################################
    main_event=None
    url_base = 'https://{main_event}.enavigator.org/api/v1/'.format(main_event=main_event)

    def __init__(self, event):
        self.main_event = event
        self.url_base = 'https://{main_event}.enavigator.org/api/v1/'.format(main_event=self.main_event)

    def get_event(self):
        response = requests.get(self.url_base + 'event', headers=self.headers_get)
        return response.json()

    def get_list(self, parent_id, expand):
        params = {'parent_id':parent_id, 'expand':expand}
        response = requests.get(self.url_base + 'list', params=params, headers=self.headers_get)
        return response.json()

    def get_participants(self, parent_id):
        params = {'parent_id':parent_id}
        response = requests.get(self.url_base + 'participants', params=params, headers=self.headers_get)
        return response.json()

    def get_flight(self, flight_id):
        if flight_id == None:
            response = requests.get(self.url_base + 'flight', headers=self.headers_get)
            return response.json()
        else:
            params = {'flight_id':flight_id}
            response = requests.get(self.url_base + 'flight', params=params, headers=self.headers_get)
            return response.json()

    def post_flight(self, flight_id):
        data = self.flight_id_struct
        data['flight_id']=flight_id
        response = requests.post(self.url_base + 'flight', json=data, headers=self.headers_post)
        return response.json()

    def post_lap(self, lap_struct):
        response = requests.post(self.url_base + 'track', json=lap_struct, headers=self.headers_post)
        return response.json()

    def post_waypoint(self, waypoint_struct):
        response = requests.post(self.url_base + 'track', json=waypoint_struct, headers=self.headers_post)
        return response.json()

    def post_accuracy(self, accuracy_struct):
        response = requests.post(self.url_base + 'track', json=accuracy_struct, headers=self.headers_post)
        return response.json()


enav = Enav('balooning')

accuracy_struct = {
    "type": "accuracy",
    "flight_id": "bdistf1",
    "transponder_id": "t101",
    "data":
        {
            "gate_id": 3,
            "distance": 666,
            "time": 4.20,
            "azimuth": 360
        }
}

print(enav.post_accuracy(accuracy_struct))