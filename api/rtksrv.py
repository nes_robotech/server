#ToDo Replace HTTP Status Codes acconding to the https://opensource.zalando.com/restful-api-guidelines/#150
from datetime import datetime
from subprocess import Popen


RTKSRV = {}
p = None

def get_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def run(flight_id, target_id, event):
    """run

    Run RTK server

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    global p

    if flight_id not in RTKSRV:
        RTKSRV[flight_id] = {}
    if target_id not in RTKSRV[flight_id]:
        RTKSRV[flight_id][target_id] = {}
        RTKSRV[flight_id][target_id]['state'] = 'init'
        RTKSRV[flight_id][target_id]['timestamp'] = get_timestamp()
        #ToDo Run server

        cmd = "python3 rtk_server.py -flight_id {flight_id} -target_id {target_id} -event {event}".format(flight_id=flight_id, target_id=target_id, event=event)
        print(cmd)
        try:
            p = Popen(cmd, shell=True)
        except Exception:
            print('RTK Server fault!')

        return {"status": "Ok"}, 200
    else:
        return {"status": "Error",
                "message": "Server already exists"}, 400


def readstate(flight_id, target_id):
    """readstate

    Retrieve a state of RTK server

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    global p

    if flight_id in RTKSRV and flight_id is not None:
        if target_id in RTKSRV[flight_id]:
            return {"status": "Ok",
                    "state": RTKSRV[flight_id][target_id]['state'],
                    "timestamp": RTKSRV[flight_id][target_id]['timestamp']}, 200
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 400
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 400


def update(flight_id, target_id, state=None):
    """update

    Update a server state

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    :param state: marker state and position
    :type state: dict | bytes
    """
    global p

    if flight_id in RTKSRV and flight_id is not None:
        if target_id in RTKSRV[flight_id]:
            #ToDo Validate server state
            if state is not None:
                RTKSRV[flight_id][target_id]['state'] = state['state']
                RTKSRV[flight_id][target_id]['timestamp'] = get_timestamp()
                return {"status": "Ok"}, 200
            else:
                return {"status": "Error",
                        "message": "Empty state"}, 400
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 400
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 400

def killsrv(flight_id, target_id):
    """killsrv

    Kill RTK server

    :param flight_id: Flight identifier
    :type flight_id: str
    :param target_id: Target identifier
    :type target_id: str
    """
    global p

    if flight_id in RTKSRV and flight_id is not None:
        if target_id in RTKSRV[flight_id]:
            del RTKSRV[flight_id][target_id]
            #ToDo Kill server
            p.kill()
            out_reader_killer = Popen("sudo pkill ../rtk/rtkrcv_out_reader.py",bufsize=0,shell=True,stdout=None)
            rtk_starter_killer = Popen("sudo pkill ../rtk/rtkrcv_starter.py",bufsize=0,shell=True,stdout=None)
            server_killer = Popen("sudo pkill rtk_server.py",bufsize=0,shell=True,stdout=None)
            return {"status": "Ok"}, 200
        else:
            return {"status": "Error",
                    "message": "Target {target_id} not exists".format(target_id=target_id)}, 400
    else:
        return {"status": "Error",
                "message": "Flight {flight_id} not exists".format(flight_id=flight_id)}, 400
