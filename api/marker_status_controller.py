import sys
sys.path.append("../cogo")
import cogo_wgs84
import argparse
import time
import requests
import json

from enav import *
import datetime

parser = argparse.ArgumentParser()
parser.add_argument("-marker_id", dest="marker_id", type=str, help="marker ID")
parser.add_argument("-flight_id", dest="flight_id", type=str, help="flight ID")
parser.add_argument("-target_id", dest="target_id", type=str, help="target ID")
parser.add_argument("-event", dest="event", type=str, help="event for enav shit")
args = parser.parse_args()
llh_file = "../rtk/MARKERS/%s.txt" % args.marker_id
llh_lable = "%s_LLH" % args.marker_id

def update(marker_id, flight_id, target_id, data):
    resp = requests.put('http://localhost:5000/api/marker/{marker_id}?flight_id={flight_id}&target_id={target_id}'.format(
        marker_id=marker_id,flight_id=flight_id, target_id=target_id), json.dumps(data), headers={"Content-Type": "application/json"})
    assert resp.json() == {'status': 'Ok'}, 'AP status update error'

f = open(llh_file,"r")
f_data = None
while True:
    data = {"state": "processing"}
    update(args.marker_id, args.flight_id, args.target_id, data)
    time.sleep(5)
    f_data = str(f.read())
    if f_data.find(llh_lable) != -1:
        f_data_list = f_data.split(':')
        az, dest = cogo_wgs84.get_azi_marker_local(float(f_data_list[1]),float(f_data_list[2]))
        data = {
                "azimuth": az,
                "distance": dest,
                "llh": {
                    "hgt": float(f_data_list[3]),
                    "lat": float(f_data_list[1]),
                    "lon": float(f_data_list[2])
                },
                "state": "ready"
            }
        update(args.marker_id, args.flight_id, args.target_id, data)
        _enav = Enav(str(args.event))#'balooning'
        accuracy_struct = {
            "type": "accuracy",
            "flight_id": str(args.flight_id),
            "transponder_id": str(args.marker_id),
            "data":
                {
                    "gate_id": int(args.target_id),
                    "distance": int(dest),
                    "time": str(datetime.datetime.utcnow()),
                    "azimuth": int(az)
                }
        }

        print(_enav.post_accuracy(accuracy_struct))
        break
"""
rtkrcv append nav
rs init sts2str start & flush .nav
ap don't go sleep

"POST /api/marker?flight_id=0000&target_id=1234&marker_id=0013 HTTP/1.1" 400 -
                                                Traceback (most r
"""
