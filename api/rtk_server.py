import sys
sys.path.append("../rtk")
sys.path.append("../tc")
from tag_control import *
from rtkrcv_starter import *
from subprocess import *
import argparse
import requests
import json
import time

parser = argparse.ArgumentParser()
parser.add_argument("-flight_id", dest="flight_id", type=str, help="flight ID")
parser.add_argument("-target_id", dest="target_id", type=str, help="target ID")
parser.add_argument("-event", dest="event", type=str, help="event for enav shit")
args = parser.parse_args()

print("RTK Server: flight_id={fid}, target_id={tid}".format(fid=args.flight_id, tid=args.target_id))
out_reader_killer = Popen("pkill ../rtk/rtkrcv_out_reader.py",bufsize=0,shell=True,stdout=None)
rtk_starter_killer = Popen("pkill ../rtk/rtkrcv_starter.py",bufsize=0,shell=True,stdout=None)
def kill_port(port):
    port_killer_cmd = "fuser -k -n tcp %s" % str(port)
    kill = Popen(port_killer_cmd, shell=True, stdin=PIPE, stdout=PIPE)
    kill.wait()

def post_marker(marker_id, flight_id, target_id, data):
    resp = requests.post('http://localhost:5000/api/marker?flight_id={flight_id}&target_id={target_id}&marker_id={marker_id}'.format(
        marker_id=marker_id,flight_id=flight_id, target_id=target_id), json.dumps(data), headers={"Content-Type": "application/json"})
    if resp.json() == {'status': 'Ok'}:
        return "OK"
    else:
        return "ERROR"

def update_marker(marker_id, flight_id, target_id, data):
    resp = requests.put('http://localhost:5000/api/marker/{marker_id}?flight_id={flight_id}&target_id={target_id}'.format(
        marker_id=marker_id,flight_id=flight_id, target_id=target_id), json.dumps(data), headers={"Content-Type": "application/json"})
    #assert resp.json() == {'status': 'Ok'}, 'Marker status update error'

def log_post(msg):
    data = {
      "flight_id": "0000",
      "message": msg,
      "sender": "RTK Server"
    }
    resp = requests.post('http://localhost:5000/api/log', json.dumps(data), headers={"Content-Type": "application/json"})
    assert resp.json() == {'status': 'Ok'}, 'AP status update error'

################################################################################
#global vars
################################################################################
rtkrcv_main_config = "../rtk/rtkrcv_main.conf"
rtkrcv_starter_cmd = "python3 ../rtk/rtkrcv_starter.py -o %s -in1 %s -in2 %s -out1 %s -g %s -ip %s -r %s -q %s -m %s -log1 %s -log2 %s -log_out %s -mode %s"
server_ip = "192.168.0.22"
server_port = "6666"
marker_new_port = 7000
Q = 1
ratio = 3.0
mid = 10
################################################################################
#str2str start
################################################################################
clean_markers = Popen("rm -rf ../rtk/MARKERS",bufsize=0,shell=True,stdout=None)
clean_markers.wait()
Popen("mkdir ../rtk/MARKERS",bufsize=0,shell=True,stdout=None)
str2str_in = server_ip + ":" + server_port
str2str_out_port = "5132"
str2str_out_addr = "127.0.0.1:%s" % str2str_out_port
#kill_port(str2str_out_port)
#str2str_kill = Popen("killall str2str",bufsize=0,shell=True,stdout=None)#PIPE True
Popen("killall rtkrcv",bufsize=0,shell=True,stdout=None)#PIPE True
#str2str_kill.wait()
str2str_cmd = "str2str -in tcpsvr://%s -out tcpsvr://:%s" % (str2str_in, str2str_out_port)
str2str = Popen(str2str_cmd,bufsize=0,shell=True,stdout=None)#PIPE True
################################################################################
#TC start
################################################################################
tc_port = 8888
tag_control = None
while(True):
    try:
        tag_control = Tag_control(server_ip, tc_port, 300)
        #data = {"state": "waiting"}
        #update(args.flight_id, args.target_id, data)
        break
    except Exception:
        kill_port(tc_port)
################################################################################
#Main
################################################################################
while(True):
    tag_control.get_tag()
    tag_ip = tag_control.get_tag_ip()
    tag_ip = ('%02X' % int(tag_ip.split(".")[2])) + ('%02X' % int(tag_ip.split(".")[3]))
    log_post("GOT_MARKER: %s" % tag_ip)
    data = {"state": "init"}
    if(post_marker(tag_ip, args.flight_id, args.target_id, data)=="OK"):
        data = {"state": "init"}
        update_marker(tag_ip, args.flight_id, args.target_id, data)
        kill_port(marker_new_port)
        tag_control.set_tag_port(marker_new_port)
        tag_new_addr = server_ip + ":" + str(marker_new_port)
        tag_control.tag_close()
        data = {"state": "waiting"}
        update_marker(tag_ip, args.flight_id, args.target_id, data)
        marker_new_port = marker_new_port + 1
        base_log = "../rtk/MARKERS/%s_BASE_LOG" % tag_ip
        marker_log = "../rtk/MARKERS/%s_MARKER_LOG" % tag_ip
        rtkpost_out = "../rtk/MARKERS/%s_OUT_LOG" % tag_ip
        llh_file = "../rtk/MARKERS/%s.txt" % tag_ip
        output_file = "../rtk/MARKERS/rtkrcv_%s_out.txt" % tag_ip
        llh_lable = "%s_LLH" % tag_ip
        marker_f = open((rtkpost_out+".pos"),"w")
        marker_f.close()
        marker_f = open(llh_file,"w")
        marker_f.close()
        marker_rtk_cmd = rtkrcv_starter_cmd % (rtkrcv_main_config, str2str_out_addr, tag_new_addr, 
                output_file, llh_file, llh_lable, ratio, Q, mid, base_log, marker_log, rtkpost_out, "fix")
        Popen(marker_rtk_cmd,bufsize=0,shell=True,stdout=PIPE)
        msc_cmd = "python3 marker_status_controller.py -marker_id %s -flight_id %s -target_id %s -event %s"%(tag_ip, args.flight_id, args.target_id, args.event)
        Popen(msc_cmd, shell=True)
    else:
        tag_control.tag_close()
