import sys
sys.path.append("../rtk")
from rtkrcv_starter import *
from subprocess import *
from time import *
import argparse
import requests
import json

parser = argparse.ArgumentParser()
parser.add_argument("-flight_id", dest="flight_id", type=str, help="flight ID")
parser.add_argument("-target_id", dest="target_id", type=str, help="target ID")
parser.add_argument("-rs_id", dest="rs_id", type=str, help="RS ID")
args = parser.parse_args()
print("RS init: flight_id={fid}, target_id={tid}, rs_id={rid}".format(fid=args.flight_id, tid=args.target_id, rid=args.rs_id))
################################################################################
#global vars
################################################################################
server_ip = "192.168.0.22"
server_port = "6666"
str2str_in = server_ip + ":" + server_port
str2str_out_port = "5132"
str2str_out_addr = "127.0.0.1:%s" % str2str_out_port

base_llh_file = "../rtk/BASE/BASE.txt"
base_output_file = "../rtk/BASE/rtkrcv_single_out.txt"
rtkrcv_single_base_config = "../rtk/rtkrcv_single_base.conf"
rtkrcv_starter_cmd = "python3 ../rtk/rtkrcv_starter.py -o %s -in1 %s -in2 %s -out1 %s -g %s -ip %s -r %s -q %s -m %s -mode %s"
base_llh_lable = "BASE_LLH"
single_rs_ratio = 0.0
single_rs_Q = 5
single_rs_mid = 20
################################################################################
#open str2str pipe
################################################################################
def kill_port(port):
    port_killer_cmd = "fuser -k -n tcp %s" % str(port)
    kill = Popen(port_killer_cmd, shell=True, stdin=PIPE, stdout=PIPE)
    kill.wait()
    
clean_base = Popen("rm -rf ../rtk/BASE",bufsize=0,shell=True,stdout=None)
clean_base.wait()
Popen("mkdir ../rtk/BASE",bufsize=0,shell=True,stdout=None)

out_reader_killer = Popen("pkill ../rtk/rtkrcv_out_reader.py",bufsize=0,shell=True,stdout=None)
rtk_starter_killer = Popen("pkill ../rtk/rtkrcv_starter.py",bufsize=0,shell=True,stdout=None)
server_killer = Popen("pkill rtk_server.py",bufsize=0,shell=True,stdout=None)
kill_port(str2str_out_port)
str2str_kill = Popen("killall str2str",bufsize=0,shell=True,stdout=None)#PIPE True
str2str_kill.wait()
Popen("killall rtkrcv",bufsize=0,shell=True,stdout=None)#PIPE True
str2str_cmd = "str2str -in tcpsvr://%s -out tcpsvr://:%s" % (str2str_in, str2str_out_port)
str2str = Popen(str2str_cmd,bufsize=0,shell=True,stdout=None)#PIPE True
print ('str2str started')
print (str2str_cmd)

def update(flight_id, target_id, data):
    resp = requests.put('http://localhost:5000/api/target/{target_id}/rs?flight_id={flight_id}'.format(
        flight_id=flight_id, target_id=target_id), json.dumps(data), headers={"Content-Type": "application/json"})
    #assert resp.json() == {'status': 'Ok'}, 'RS status update error'
    print(resp.json())

data = {"state": "waiting"}
update(args.flight_id, args.target_id, data)
################################################################################
#rs init process
################################################################################

single_rs_f = open(base_llh_file,"w")
single_rs_f.close()
rs_init_cmd = rtkrcv_starter_cmd % (rtkrcv_single_base_config, str2str_out_addr, str2str_out_addr, base_output_file, base_llh_file, base_llh_lable, single_rs_ratio, single_rs_Q, single_rs_mid, "single")
print (rs_init_cmd)
rs_init_proc = Popen(rs_init_cmd,bufsize=0,shell=True,stdout=PIPE)
single_rs_f = open(base_llh_file,"r")
single_rs_f_data = None
while True:
    #pass
    data = {"state": "processing"}
    update(args.flight_id, args.target_id, data)
    sleep(5)
    single_rs_f_data = str(single_rs_f.read())
    if single_rs_f_data.find(base_llh_lable) != -1:
        single_rs_f_data_list = single_rs_f_data.split(':')
        data = {
                "llh": {
                    "hgt": float(single_rs_f_data_list[3]),
                    "lat": float(single_rs_f_data_list[1]),
                    "lon": float(single_rs_f_data_list[2])
                },
                "state": "ready"
            }
        update(args.flight_id, args.target_id, data)
        rs_init_proc.kill()
        break
