import sys
sys.path.append("../rtk")
sys.path.append("../tc")
from tag_control import *
from rtkrcv_starter import *
from subprocess import *
import argparse
import requests
import json
import time

parser = argparse.ArgumentParser()
parser.add_argument("-flight_id", dest="flight_id", type=str, help="flight ID")
parser.add_argument("-target_id", dest="target_id", type=str, help="target ID")
parser.add_argument("-marker_id", dest="marker_id", type=str, help="marker ID")
args = parser.parse_args()
print("AP init: flight_id={fid}, target_id={tid}, marker_id={mid}".format(fid=args.flight_id, tid=args.target_id, mid=args.marker_id))
################################################################################
#global vars
################################################################################
server_ip = "192.168.0.22"
server_port = "6666"
str2str_in = server_ip + ":" + server_port
str2str_out_port = "5132"
str2str_out_addr = "127.0.0.1:%s" % str2str_out_port

tc_port = 8888

ap_base_log = "../rtk/AP/AP_BASE_LOG"
ap_marker_log = "../rtk/AP/AP_MARKER_LOG"
ap_rtkpost_out = "../rtk/AP/AP_OUT_LOG"
ap_llh_file = "../rtk/AP/AP.txt"
ap_output_file = "../rtk/AP/rtkrcv_ap_out.txt"
rtkrcv_main_config = "../rtk/rtkrcv_main.conf"
rtkrcv_starter_cmd = "python3 ../rtk/rtkrcv_starter.py -o %s -in1 %s -in2 %s -out1 %s -g %s -ip %s -r %s -q %s -m %s -log1 %s -log2 %s -log_out %s -mode %s"
ap_llh_lable = "AP_LLH"
ap_ratio = 3.0
ap_Q = 1
ap_mid = 10

def kill_port(port):
    port_killer_cmd = "fuser -k -n tcp %s" % str(port)
    kill = Popen(port_killer_cmd, shell=True, stdin=PIPE, stdout=PIPE)
    kill.wait()

def update(flight_id, target_id, data):
    resp = requests.put('http://localhost:5000/api/target/{target_id}/ap?flight_id={flight_id}'.format(
        flight_id=flight_id, target_id=target_id), json.dumps(data), headers={"Content-Type": "application/json"})
    assert resp.json() == {'status': 'Ok'}, 'AP status update error'

clean_ap = Popen("rm -rf ../rtk/AP",bufsize=0,shell=True,stdout=None)
clean_ap.wait()
Popen("mkdir ../rtk/AP",bufsize=0,shell=True,stdout=None)
#kill_port(str2str_out_port)
#str2str_kill = Popen("killall str2str",bufsize=0,shell=True,stdout=None)#PIPE True
rtkrcvkiller = Popen("killall rtkrcv",bufsize=0,shell=True,stdout=None)#PIPE True
rtkrcvkiller.wait()
#str2str_kill.wait()
str2str_cmd = "str2str -in tcpsvr://%s -out tcpsvr://:%s" % (str2str_in, str2str_out_port)
str2str = Popen(str2str_cmd,bufsize=0,shell=True,stdout=None)#PIPE True
first = args.marker_id[0] + args.marker_id[1]
second = args.marker_id[2] + args.marker_id[3]
ap_marker_addr = "192.168." + str(int(first, 16)) + "." + str(int(second, 16))
ap_port = 6999
while(True):
    try:
        tag_control = Tag_control(server_ip, tc_port, 300)
        data = {"state": "waiting"}
        update(args.flight_id, args.target_id, data)
        tag_control.get_tag()
        tag_ip = tag_control.get_tag_ip()
        kill_port(ap_port)
        tag_control.set_tag_port(ap_port)
        tag_new_addr = server_ip + ":" + str(ap_port)
        tag_control.tag_close()
        break
    except Exception:
        kill_port(tc_port)


ap_marker_addr=server_ip + ":" + str(ap_port)
################################################################################
#ap init process
################################################################################
ap_f = open((ap_rtkpost_out+".pos"),"w")
ap_f.close()
ap_f = open(ap_llh_file,"w")
ap_f.close()
ap_init_cmd = rtkrcv_starter_cmd % (rtkrcv_main_config, str2str_out_addr, ap_marker_addr,
        ap_output_file, ap_llh_file, ap_llh_lable, ap_ratio, ap_Q, ap_mid, ap_base_log, ap_marker_log, ap_rtkpost_out, "fix")
#print (cp_init_cmd)
ap_init_proc = Popen(ap_init_cmd,bufsize=0,shell=True,stdout=PIPE)
ap_f = open(ap_llh_file,"r")
ap_f_data = None

while True:
    data = {"state": "processing"}
    update(args.flight_id, args.target_id, data)
    time.sleep(5)
    ap_f_data = str(ap_f.read())
    if ap_f_data.find(ap_llh_lable) != -1:
        ap_f_data_list = ap_f_data.split(':')
        data = {
                "llh": {
                    "hgt": float(ap_f_data_list[3]),
                    "lat": float(ap_f_data_list[1]),
                    "lon": float(ap_f_data_list[2])
                },
                "state": "ready"
            }
        update(args.flight_id, args.target_id, data)
        ap_init_proc.wait()#was kill()
        break
