import sys
sys.path.append("../tcp")
from tcp_server import *
from time import *
#server which proceed tags
################################################################################
class Tag_control(object):
    tag_srv = None
    tag_ip = None
    tag_ip_port = None
    tag_port = None
    tag_status = None
    tag_addr = None
    def __init__(self, _host, _port, _max_cli):
        self.tag_srv = Server_tcp(_host, _port)
        self.tag_srv.server_listen(_max_cli)

    def get_tag(self):
        self.tag_addr = self.tag_srv.server_accept()
        #print ("Get new tag")

    def get_tag_ip(self):
        while True:
            self.tag_srv.server_send("GET_IP")
            self.tag_ip = self.tag_srv.server_rcv()
            #print(self.tag_ip)
            if self.tag_ip != "error":
                break;
        #print ("Client IP: " + str(self.tag_ip))
        return self.tag_ip

    def set_tag_port(self, _port):#_host_port = "127.0.0.1:7070"
        while True:
            self.tag_srv.server_send("SET_PORT:" + str(_port))
            self.tag_port = self.tag_srv.server_rcv()
            #print(self.tag_port)
            if str(self.tag_port) == str(_port):
                break;

    def set_tag_data_type(self, _data_type):#DATA_TYPE:UBX
        while True:
            self.tag_srv.server_send(str(_data_type)+"\r\n")
            #print (self.tag_srv.server_rcv())
            self.tag_srv.server_send("START\n\r")
            #print (self.tag_srv.server_rcv())
            sleep(10)

    def tag_close(self):
        while True:
            self.tag_srv.server_send("CLOSE")
            self.tag_status = self.tag_srv.server_rcv()
            #print(self.tag_status)
            if self.tag_status == "CLOSED":
                self.tag_srv.close_cli()
                break;
        #print ("tag status: CLOSED")

    def tag_control_close(self):
        self.tag_srv.server_close
################################################################################

def test():
    test_srv = Tag_control("127.0.0.1", 6062, 300)
    while True:
        test_srv.get_tag()
        test_srv.get_tag_id()
        test_srv.set_tag_conf("127.0.0.1:7070")
        test_srv.tag_close()
