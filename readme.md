#Requirments

#Web API

python3

It has to be added to `requirements.txt`
```
Flask==1.0.2
connexion==1.5.2
```
Manual installation
```
pip3 install flask
pip3 install connexion
pip3 install flask-cors
```

To install PYPROJ:

* clone github repo or download source release at http://python.org/pypi/pyproj.
* `python3 setup.py build`
* `python3 setup.py install` (with sudo if necessary).